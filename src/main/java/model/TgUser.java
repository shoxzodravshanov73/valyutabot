package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TgUser {
    private long chatId;
    private BotState botState;
    private String fromCurrency;
    private  String toCurrency;


}
