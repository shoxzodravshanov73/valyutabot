package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.special.Currencys;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Servise {

    static List<TgUser>users=new ArrayList<>();

    public  static SendMessage start(Update update){
        SendMessage sendMessage=new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Valyutani tanlang : ");
        InlineKeyboardMarkup inlineKeyboardMarkup=new InlineKeyboardMarkup();


        List<List<InlineKeyboardButton>> inlineRows=new ArrayList<>();
        List<InlineKeyboardButton> inlineRow=new ArrayList<>();

        InlineKeyboardButton button=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDF8USD➡️SO'M\uD83C\uDDFA\uD83C\uDDFF");
        button.setCallbackData("USD/SUM");
        InlineKeyboardButton button1=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡️USD\uD83C\uDDFA\uD83C\uDDF8");
        button1.setCallbackData("SUM/USD");

        inlineRow.add(button);
        inlineRow.add(button1);

        List<InlineKeyboardButton> inlineRow1=new ArrayList<>();

        InlineKeyboardButton button2=new InlineKeyboardButton("\uD83C\uDDEA\uD83C\uDDFAEURO➡SO'M\uD83C\uDDFA\uD83C\uDDFF");
       button2.setCallbackData("EUR/SUM");
        InlineKeyboardButton button3=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡EURO\uD83C\uDDEA\uD83C\uDDFA");
        button3.setCallbackData("SUM/EUR");


        inlineRow1.add(button2);
        inlineRow1.add(button3);

        List<InlineKeyboardButton> inlineRow2=new ArrayList<>();

        InlineKeyboardButton button4=new InlineKeyboardButton("\uD83C\uDDE8\uD83C\uDDF3YUAN➡SO'M\uD83C\uDDFA\uD83C\uDDFF");
        button4.setCallbackData("CNY/SUM");
        InlineKeyboardButton button5 = new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡️YUAN\uD83C\uDDE8\uD83C\uDDF3");
        button5.setCallbackData("SUM/CNY");

        inlineRow2.add(button4);
        inlineRow2.add(button5);

        inlineRows.add(inlineRow);
        inlineRows.add(inlineRow1);
        inlineRows.add(inlineRow2);

        inlineKeyboardMarkup.setKeyboard(inlineRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        sendMessage.setChatId(update.getMessage().getChatId());


        return sendMessage;
    }

    public static TgUser findUser(Update update){
        long chatId=update.getCallbackQuery()!=null ?
                update.getCallbackQuery().getMessage().getChatId():
                update.getMessage().getChatId();

        for (TgUser user : users) {
            if (user.getChatId()==chatId){
                return  user;
            }
        }
        TgUser tgUser=new TgUser(chatId,BotState.CHOOSE_CURRENCY,null,null);
         users.add(tgUser);
         return tgUser;
    }

    public static SendMessage getCurrency(Update update){
        SendMessage sendMessage=new SendMessage();
        long chatId=update.getCallbackQuery().getMessage().getChatId();
        String currency=update.getCallbackQuery().getData();
        String fromCurrency=currency.substring(0,currency.indexOf("/"));
        String toCurrency=currency.substring(currency.indexOf("/")+1);

        TgUser user =findUser(update);
        user.setFromCurrency(fromCurrency);
        user.setToCurrency(toCurrency);
        user.setBotState(BotState.ENTER_AMOUNT);

        updateUser(user);
        sendMessage.setText("Miqdorni kiriting");
        sendMessage.setChatId(chatId);
        return sendMessage;
    }
    public static void updateUser(TgUser user){
        for (TgUser tgUser : users) {
            if (user.getChatId()==tgUser.getChatId()){
                tgUser=user;
                break;
            }
        }
    }
    public static SendMessage getAmount(Update update) throws IOException {
        SendMessage sendMessage =new SendMessage();

        double amount =Double.parseDouble(update.getMessage().getText());
        TgUser user=findUser(update);

        String fromCurrency= user.getFromCurrency();
        String toCurrency= user.getToCurrency();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();



        URL    url = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/"+(fromCurrency.equals("SUM")?toCurrency:fromCurrency)+"/");
            URLConnection urlConnection= url.openConnection();
            BufferedReader reader=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Currencys[] curr = gson.fromJson(reader, Currencys[].class);

            double result;
            if (fromCurrency.equals("SUM")){;
                result=amount/(Double.parseDouble(curr[0].getRate()));
            }
            else{
                result=amount*(Double.parseDouble(curr[0].getRate()));
            }
            sendMessage.setText(amount+"  "+fromCurrency+"  "+result+"  "+toCurrency);





//        InlineKeyboardMarkup inlineKeyboardMarkup=new InlineKeyboardMarkup();
////        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
//
//        List<List<InlineKeyboardButton>> inlineRows=new ArrayList<>();
//        List<InlineKeyboardButton> inlineRow=new ArrayList<>();
//
//        InlineKeyboardButton button=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDF8USD➡️SO'M\uD83C\uDDFA\uD83C\uDDFF");
//        button.setCallbackData("USD/SUM");
//        InlineKeyboardButton button1=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡️USD\uD83C\uDDFA\uD83C\uDDF8");
//        button1.setCallbackData("SUM/USD");
//
//        inlineRow.add(button);
//        inlineRow.add(button1);
//
//        List<InlineKeyboardButton> inlineRow1=new ArrayList<>();
//
//        InlineKeyboardButton button2=new InlineKeyboardButton("\uD83C\uDDEA\uD83C\uDDFAEURO➡SO'M\uD83C\uDDFA\uD83C\uDDFF");
//        button2.setCallbackData("EUR/SUM");
//        InlineKeyboardButton button3=new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡EURO\uD83C\uDDEA\uD83C\uDDFA");
//        button3.setCallbackData("SUM/EUR");
//
//
//        inlineRow.add(button2);
//        inlineRow.add(button3);
//
//        List<InlineKeyboardButton> inlineRow2=new ArrayList<>();
//
//        InlineKeyboardButton button4=new InlineKeyboardButton("\uD83C\uDDE8\uD83C\uDDF3YUAN➡SO'M\uD83C\uDDFA\uD83C\uDDFF");
//        button4.setCallbackData("CNY/SUM");
//        InlineKeyboardButton button5 = new InlineKeyboardButton("\uD83C\uDDFA\uD83C\uDDFFSO'M➡️YUAN\uD83C\uDDE8\uD83C\uDDF3");
//        button5.setCallbackData("SUM/CNY");
//
//        inlineRow.add(button4);
//        inlineRow.add(button5);
//
//        inlineRows.add(inlineRow);
//        inlineRows.add(inlineRow1);
//        inlineRows.add(inlineRow2);
//
//        inlineKeyboardMarkup.setKeyboard(inlineRows);
//        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        sendMessage.setChatId(update.getMessage().getChatId());
        return sendMessage;

    }
}
//(fromCurrency.equals("SUM")?toCurrency:fromCurrency)